/**
 * Created by krysn on 28.06.2017.
 */
import { Injectable} from '@angular/core';
import {Http, Response, Headers, RequestOptions} from '@angular/http';
import '../../node_modules/rxjs/Rx';
import {Observable} from 'rxjs/Rx';


@Injectable()
export class MailerService{
  constructor(private http: Http){}

  testApi() : Observable<any> {
    // ...using get request
    return this.http.get('http://localhost:7878/api')
    // ...and calling .json() on the response to return data
      .map((res:Response) => res.json())
      //...errors if any
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));
  }

  sentMail(body: Object) : Observable<any>{
    let bodyString = JSON.stringify(body); // Stringify payload
    let headers      = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options       = new RequestOptions({ headers: headers }); // Create a request option
    // let url = 'http://localhost:5000/api/contactForm';
    let url = 'https://sevenam-mailer.herokuapp.com/api/contactForm';
    return this.http.post(url,body, options)
      .map((res:Response) => res.json())
      //...errors if any
      .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  }
}
