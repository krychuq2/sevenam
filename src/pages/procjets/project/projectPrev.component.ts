/**
 * Created by krysn on 16.07.2017.
 */
/**
 * Created by krysn on 22.06.2017.
 */
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ShareButton, ShareProvider} from "ngx-sharebuttons";

@Component({
  selector: 'projectPrev',
  templateUrl: 'projectPrev.component.html',
  styleUrls: ['projectPrev.scss', '../../../styles/general.scss']

})
export class ProjectPrev implements OnInit{
  // fbButton;
  fbTemp = "<img src='../../../assets/images/projects/facabook-share.png'>";

  ngOnInit(): void {
    // this.fbButton = new ShareButton(
    //   ShareProvider.FACEBOOK,
    //   '<i class="fa fa-facebook"></i>',
    //   'facebook'
    // );
  }

  isLoaded:Boolean;
  pictureName:String;
  constructor(private routeParams: ActivatedRoute) {
    this.isLoaded = false;
    this.routeParams.params.subscribe(params => {
      this.pictureName = params['prev'];
    });  }

  dosomething(){
    this.isLoaded = true;
  }

}
