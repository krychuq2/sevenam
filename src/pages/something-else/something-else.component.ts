/**
 * Created by krysn on 22.06.2017.
 */
import {Component} from '@angular/core';

@Component({
  selector: 'something-else',
  templateUrl: 'something-else.component.html',
  styleUrls: ['something-else.scss', '../../styles/general.scss']

})
export class SomethingElse {

  constructor(){
  }


}
