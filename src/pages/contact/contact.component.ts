/**
 * Created by krysn on 25.06.2017.
 */
/**
 * Created by krysn on 22.06.2017.
 */
import {Component} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {MailerService} from "../../services/mailer.service";
import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'contact',
  templateUrl: 'contact.component.html',
  styleUrls: ['contact.scss', '../../styles/general.scss']

})
export class Contact {
  contactForm: FormGroup;
  nameControl;
  emailControl;
  messageControl;
  isProcessing: boolean;
  isSent : boolean;
  EMAIL_PATTERN = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

  constructor(private formBuilder : FormBuilder, private mailerService : MailerService, private trans: TranslateService){
    this.buildForm();
    this.isProcessing = false;
    this.isSent = false;

  }

  public onSubmitForm(){
    this.isProcessing = true;
    var data ={
      name : this.nameControl.value,
      from : this.emailControl.value,
      message : this.messageControl.value,
      language : this.trans.currentLang
    };
    this.mailerService.sentMail(data).subscribe(
      success => {
        this.isSent = true;
        this.isProcessing = false;
        this.contactForm.reset();
      },
      err => {
        this.isProcessing = false;
        this.contactForm.reset();

      }
    )
  }
  private buildForm(){
    this.contactForm = this.formBuilder.group({
      name  : this.formBuilder.control(null, [Validators.required, Validators.minLength(3)]),
      email : this.formBuilder.control(null, [Validators.pattern(this.EMAIL_PATTERN), Validators.required]),
      message : this.formBuilder.control(null, [Validators.required, Validators.minLength(10)])
    });

    this.nameControl = this.contactForm.get('name');
    this.emailControl = this.contactForm.get('email');
    this.messageControl = this.contactForm.get('message');
  }


}
