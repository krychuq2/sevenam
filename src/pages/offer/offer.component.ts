/**
 * Created by krysn on 22.06.2017.
 */
import {Component} from '@angular/core';
import {TranslateService} from "ng2-translate";

@Component({
  selector: 'offer',
  templateUrl: 'offer.component.html',
  styleUrls: ['../../styles/general.scss', 'offer.scss']

})
export class Offer {

  constructor(public trans: TranslateService){
    console.log(this.trans.currentLang, '<---- offer');
  }


}
