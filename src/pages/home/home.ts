/**
 * Created by krysn on 22.06.2017.
 */
import {Component, OnDestroy, OnInit} from '@angular/core';



@Component({
  templateUrl: 'home.html',
  styleUrls: ['home.scss', '../../styles/general.scss']

})
export class Home implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    document.getElementById("home").classList.remove("active");
  }

  ngOnInit(): void {
    document.getElementById("home").classList.add("active");
  }

}
