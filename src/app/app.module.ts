import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {TopNav} from "../components/topnav/topnav.component";
import {Routes, RouterModule} from "@angular/router";
import {Offer} from "../pages/offer/offer.component";
import {Home} from "../pages/home/home";
import {Story} from "../pages/story/story.component";
import {Whatdowedo} from "../pages/whatdowedo/whatdowedo.component";
import {Contact} from "../pages/contact/contact.component";
import {FullPackage} from "../pages/full-package/full-package.component";
import {SinglePackage} from "../pages/single-package/single-package.component";
import {SomethingElse} from "../pages/something-else/something-else.component";
import {ReactiveFormsModule} from "@angular/forms";
import { Autosize } from '../../node_modules/angular2-autosize/angular2-autosize';
import {MailerService} from "../services/mailer.service";
import { HttpModule } from '@angular/http';
import {Projects} from "../pages/procjets/projects.component";
import { CarouselModule } from '../../node_modules/ngx-carousel';
import {Carousel} from "../components/carousel/carousel.component";
import { TranslateModule } from 'ng2-translate';
import {ProjectPrev} from "../pages/procjets/project/projectPrev.component";
import {Angulartics2GoogleAnalytics, Angulartics2Module} from "angulartics2";
import {ShareButtonsModule} from 'ngx-sharebuttons';

const appRoutes: Routes = [
  {path: 'our-offer', component: Offer},
  {path: '', component: Home},
  {path: 'our-story', component: Story},
  {path: 'what-do-we-do', component: Whatdowedo},
  {path: 'contact', component: Contact},
  {path: 'full-package', component: FullPackage},
  {path: 'single-package', component: SinglePackage},
  {path: 'something-else', component: SomethingElse},
  {path: 'projects', component: Projects},
  {path: 'project', component: ProjectPrev}


];

@NgModule({
  declarations: [
    AppComponent,
    TopNav,
    Offer,
    Home,
    Story,
    Whatdowedo,
    Contact,
    Autosize,
    FullPackage,
    SinglePackage,
    SomethingElse,
    Projects,
    Carousel,
    ProjectPrev

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpModule,
    CarouselModule,
    TranslateModule.forRoot(),
    Angulartics2Module.forRoot([ Angulartics2GoogleAnalytics ]),
    Angulartics2Module.forChild(),

    ShareButtonsModule.forRoot()



  ],
  providers: [MailerService],
  bootstrap: [AppComponent, TopNav]
})
export class AppModule {angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics }
