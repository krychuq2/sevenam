
import {Component} from '@angular/core';
import { TranslateService } from 'ng2-translate';
import { Angulartics2GoogleAnalytics } from 'angulartics2';

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(private trans: TranslateService,
              private angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics ) {
    trans.addLangs(["en", "pl"]);
    trans.setDefaultLang('en');

    let browserLang = trans.getBrowserLang();
    trans.use(browserLang.match(/en|pl/) ? browserLang : 'en');

  }
  changeLang(lang: string) {
    this.trans.use(lang);
  }
}
