import { Component, OnInit} from '@angular/core';

@Component({
  selector: 'carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']

})
export class Carousel implements OnInit{
  public carouselBannerItems: Array<any>;
  public carouselBanner: Object;

  //booky-app
  ngOnInit(): void {

    this.carouselBannerItems = [
      {class : "project",  param : {prev: 'booky-app'}},
      {class : "project one", param : {prev: 'otis'}},
      {class : "project two", param : {prev: 'the-bug'}},
      {class : "project three", param : {prev: 'geometry-and-numbers'}},
      {class : "project four", param : {prev: 'before-you-say-anything'}},
      {class : "project five", param : {prev: 'sketch-the-world'}}];

    this.carouselBanner = {
      'items': '1-2-3-4',
      'slide': 1,
      'speed': 400,
      'interval': 150000,
      'animation': '',
      'point': true,
      'load': 2,
      'custom': 'banner'
    }
  }

  public carouselBannerLoad(evt: any) {


  // const len = this.carouselBannerItems.length;
  // if (len <= 6) {
  //   for (let i = len; i < len + 6; i++) {
  //     this.carouselBannerItems.push(i);
  //   }
  // }

}

}
