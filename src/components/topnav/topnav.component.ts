/**
 * Created by krysn on 22.06.2017.
 */
import {Component, ElementRef, Renderer, Renderer2, ViewChild} from '@angular/core';
import { TranslateService } from 'ng2-translate';
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'top-nav',
  templateUrl: './topnav.component.html',
  styleUrls: ['./topnav.scss']

})
export class TopNav {
  @ViewChild("navUl") ul:ElementRef;

  navBurger:boolean;

  constructor(private trans: TranslateService, private rd: Renderer2) {
      this.navBurger = false;
   }
  public openBurger(){
    this.navBurger =! this.navBurger;
  }

  changeLang(lang: string) {
    this.trans.use(lang);
  }

}
