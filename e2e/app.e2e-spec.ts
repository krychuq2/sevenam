import { SevenamPage } from './app.po';

describe('sevenam App', () => {
  let page: SevenamPage;

  beforeEach(() => {
    page = new SevenamPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
